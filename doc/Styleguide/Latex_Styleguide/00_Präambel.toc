\babel@toc {ngerman}{}
\contentsline {chapter}{\numberline {1}Python}{3}{chapter.1}%
\contentsline {section}{\numberline {1.1}Formulierung}{3}{section.1.1}%
\contentsline {subsection}{\numberline {1.1.1}Namensgebung}{3}{subsection.1.1.1}%
\contentsline {subsection}{\numberline {1.1.2}Variablendefinition}{4}{subsection.1.1.2}%
\contentsline {section}{\numberline {1.2}Formatierung}{4}{section.1.2}%
\contentsline {subsection}{\numberline {1.2.1}Semikolon}{4}{subsection.1.2.1}%
\contentsline {subsection}{\numberline {1.2.2}Zeilenlänge}{4}{subsection.1.2.2}%
\contentsline {subsection}{\numberline {1.2.3}Einrückungen}{5}{subsection.1.2.3}%
\contentsline {subsection}{\numberline {1.2.4}Leerzeilen}{5}{subsection.1.2.4}%
\contentsline {subsection}{\numberline {1.2.5}Leerzeichen}{5}{subsection.1.2.5}%
\contentsline {chapter}{\numberline {2}HTML}{6}{chapter.2}%
\contentsline {section}{\numberline {2.1}Allgemeine Formatierung}{6}{section.2.1}%
\contentsline {section}{\numberline {2.2}HTML-Zeilenumbruch}{6}{section.2.2}%
\contentsline {section}{\numberline {2.3}HTML-Anführungszeichen}{7}{section.2.3}%
\contentsline {chapter}{\numberline {3}JavaScript}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}Klammern}{8}{section.3.1}%
\contentsline {section}{\numberline {3.2}Spaltengrenze}{8}{section.3.2}%
\contentsline {section}{\numberline {3.3}Zeilenumbruch}{9}{section.3.3}%
\contentsline {section}{\numberline {3.4}Dateiname}{9}{section.3.4}%
\contentsline {chapter}{\numberline {4}Quellenverzeichnis}{10}{chapter.4}%
\contentsline {section}{\numberline {4.1}Python}{10}{section.4.1}%
\contentsline {section}{\numberline {4.2}HTML}{10}{section.4.2}%
\contentsline {section}{\numberline {4.3}JavaScript}{10}{section.4.3}%
\providecommand \tocbasic@end@toc@file {}\tocbasic@end@toc@file 
