CREATE TABLE bewaesserung(
	`ID` INT NOT NULL AUTO_INCREMENT,
	`TimeStamp` DATETIME DEFAULT NOW(),
	`Message` TEXT,
	`Topic` TEXT,
	`User` TEXT,
	`Password` TEXT,
	`e-Mail` TEXT,
	`Temperatur` DECIMAL(2,1),
	`FromTimeB1` TIME,
	`TilTimeB1` TIME, 
	`FromTimeB2` TIME,
	`TilTimeB2` TIME, 
	`FromTimeB3` TIME,
	`TilTimeB3` TIME, 
	`AreaPic1` BLOB,bewaesserung
	`AreaPic2` BLOB,
	`AreaPic3` BLOB,
	
	PRIMARY KEY (`ID`)
);
