#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: AutomaticController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält die Klasse des Automatikmodus
#------------------------------------------------------------

#Space for imports 
import time
from DatabaseController import DatabaseManager

class AutomaticManager:
   
    dbMan = DatabaseManager()

    # Funktion liefert aktuelle und zeit und formatiert in string
    # Parameter: keine
    # Rückgabe: aktuelle Zeit als string
    def getTimeNow(self):
        timenow = time.localtime()
        timenow = time.strftime("%H:%M")
        return timenow

    # Prüft ob die aktuelle Zeit gleich "vonZeit" aus Bewaesserung
    # Parameter: VonZeit aus Bewaesserung 
    # True wenn zeit gleich
    def timeCompareFromTime(self, fromTime):
        if (self.getTimeNow()==fromTime):
            return True
        else:
            return False

    # Prüft ob die aktuelle Zeit gleich "BisZeit" aus Bewaesserung
    # Parameter: VonZeit aus Bewaesserung 
    # True wenn zeit gleich
    def timeCompareTilTime(self, tilTime):
        if (self.getTimeNow()==tilTime):
            return True 
        else: 
            return False

    # Zusammen gesezte Logik für die Automatik funktion 
    # vergleicht die Zeiten und gibt status für den jeweiligen Bereich zurück  
    # Parameter: area (jeweiligen Bereich den man vergleichen möchte)
    # Rückgabe: status: 1 = soll angeschaltet werden
    #           status: 2 = soll abgeschaltet werden
    #           status: 0 = soll nichts passieren
    def mainfunction(self, area):
        if (area == 1): 
            fromTimeB1 = self.dbMan.getTime("FromTimeB1")
            fromTimeB1 = str(fromTimeB1)
            tilTimeB1 = self.dbMan.getTime("TilTimeB1")
            tilTimeB1 = str(tilTimeB1)
            if(self.timeCompareFromTime(fromTimeB1)==True):
                return 1 
            elif(self.timeCompareTilTime(tilTimeB1)==True):
                return 2
            else:
                return 0

        elif (area == 2):
            fromTimeB2 = self.dbMan.getTime("FromTimeB2")
            fromTimeB2 = str(fromTimeB2)
            tilTimeB2 = self.dbMan.getTime("TilTimeB2")
            tilTimeB2 = str(tilTimeB2)
            if(self.timeCompareFromTime(fromTimeB2)==True):
                return 1 
            elif(self.timeCompareTilTime(tilTimeB2)==True):
                return 2
            else:
                return 0
            
        elif (area == 3):
            fromTimeB3 = self.dbMan.getTime("FromTimeB3")
            fromTimeB3 = str(fromTimeB3)
            tilTimeB3 = self.dbMan.getTime("TilTimeB3")
            tilTimeB3 = str(tilTimeB3)
            if (self.timeCompareFromTime(fromTimeB3)==True):
                return 1 
            elif (self.timeCompareTilTime(tilTimeB3)==True):
                return 2
            else:
                return 0


