#--------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: DatabaseController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält Klasse für Funktionalitäten mit Datenbank 
#--------------------------------------------------------------
import mariadb
from datetime import date, timedelta


# Klasse für alle Interaktionen mit der Datenbank
# Funktionalitäten: DB Verbindung aufbauen
#                   DB lesen (Email)
#                   DB lesen (Zeitstempel)
#                   DB lesen (IDs)
#                   DB lesen (manueller Modus)
#                   DB schreiben Temperaturwerte
#                   DB löschen (Temperaturen älter 4 Wochen)
class DatabaseManager:
    try:
        connection = mariadb.connect(
                    user="root",
                    password="123456",
                    host="localhost",
                    port=3306,
                    database="bewaesserung"
                ) 
        cursor = connection.cursor(dictionary=True)
        
    except mariadb.Error as error:
        print("Error connecting to mariadb: {error}")
    
    # Holt die E-Mail Adresse aus der Datenbank
    # übergebene Parameter: Keine 
    # Parameter: zahl = Initialwert der ersten zu verwendenden ID 
    #            i = Iterator um ID-Wert zu erhöhen 
    #            listofMails = leerer String, in den Mails reingeschrieben werden
    #            mailElement = fängt Mailadresse aus DB ab 
    #            mailElementString = alle Mailadressen als ein String, verbunden mit §-Zeichen
    # Rückgabewert: Gibt alle E-Mail-Adressen als einen String zurück
    def getMail(self):
        self.cursor.execute("SELECT Mail FROM bewaesserung")
        arrayOfMails = list(map(lambda x: x.get("Mail"), self.cursor.fetchall()))
        return(arrayOfMails)

    # Holt Anfangs und Endzeit der einzelnen Bereiche im Automatikmodus 
    # übergebene Parameter: Startzeit oder Endzeit von bestimmtem Bereich (areaStartOrEnd) 
    # Rückgabewert: Anfangs oder Endzeit des angeforderten Bereichs 
    def getTime(self, areaStartOrEnd):
        if (areaStartOrEnd == "FromTimeB1"): 
            self.cursor.execute("SELECT FromTimeB1 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("FromTimeB1"), self.cursor.fetchall()))
            return time[0]
        elif (areaStartOrEnd == "TilTimeB1"): 
            self.cursor.execute("SELECT TilTimeB1 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("TilTimeB1"), self.cursor.fetchall()))
            return time[0]
        elif (areaStartOrEnd == "FromTimeB2"): 
            self.cursor.execute("SELECT FromTimeB2 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("FromTimeB2"), self.cursor.fetchall()))
            return time[0]
        elif (areaStartOrEnd == "TilTimeB2"): 
            self.cursor.execute("SELECT TilTimeB2 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("TilTimeB2"), self.cursor.fetchall()))
            return time[0]
        elif (areaStartOrEnd == "FromTimeB3"): 
            self.cursor.execute("SELECT FromTimeB3 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("FromTimeB3"), self.cursor.fetchall()))
            return time[0]
        elif (areaStartOrEnd == "TilTimeB3"): 
            self.cursor.execute("SELECT TilTimeB3 FROM bewaesserung where ID=355")
            time = list(map(lambda x: x.get("TilTimeB3"), self.cursor.fetchall()))
            return time[0]

    # Schreibt Temperaturwerte in die Datenbank  
    # übergebene Parameter: aktuellen Temperaturwert (tempNow) 
    # Rückgabewert: keiner
    def pushTemp(self, tempNow): 
        dateToday = date.today() 
        dateToday = dateToday.strftime("%Y%m%d")
        self.cursor.execute(f"INSERT INTO timetemp (Datum, Temperatur) VALUES (?, ?)", (dateToday, tempNow))
        self.connection.commit()


    # Löscht Daten aus der Temperaturtabelle die älter als 30 Tage sind
    # Übergabe Parameter: keine 
    # Parameter: dates = array der Datums aus DB  
    # Rückgabe: keine
    # Sonstiges: Ruft dateCompare() auf
    def deleteData(self):
        self.cursor.execute("SELECT Datum FROM timetemp")
        dates = list(map(lambda x: x.get("Datum"), self.cursor.fetchall()))
       
        for date in dates:    
            datecompare = self.dateCompare(date)
            
            if (datecompare == True):
                date = date.strftime("%Y%m%d")
                self.cursor.execute(f"DELETE FROM timetemp WHERE Datum={date}")
                self.connection.commit()
            else:
                continue


    # Vergleicht ob das Datum älter als 30 Tage ist 
    # Übergabe Parameter: Zeitstempel aus DB
    # Rückgabe: True wenn älter als 30 Tage
    def dateCompare(self, dateDB):
        dateToday = date.today() 

        if (dateToday-timedelta(days=30) > dateDB):
            return True
        else:
            return False


    # Liest IDs aus Tabelle, in der Temperaturdaten gespeichert werden    
    # übergebene Parameter: keine  
    # Parameter: IDs  = fängt ausgelesene Daten auf 
    # Rückgabewert: IDs 
    def getIDs(self):
        self.cursor.execute("SELECT ID FROM bewaesserung")
        test = list(map(lambda x: x.get("ID"), self.cursor.fetchall()))
        return(test)
    
    # Holt den letzten Temperatureintrag aus der Datenbank 
    # Parameter: keine
    # Rückgabewert: Letzte Temperatur
    def getLastTemp(self):
        self.cursor.execute("SELECT Temperatur, MAX(ID) FROM timetemp")
        lastTemp = list(map(lambda x: x.get("Temperatur"), self.cursor.fetchall()))
        return(lastTemp)

