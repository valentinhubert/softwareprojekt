#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: GPIOController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält  Klasse des GPIO-Managers, Interaktion mit Hardware 
#------------------------------------------------------------
import random
from sense_hat import SenseHat
from gpiozero import LED 

# Klasse zum Abfragen der Temperaturwerte (vom Sensor)  
# Funktionalitäten: liest Temperatursensor aus 
#                   liest den Joystick aus 
#                   steuert Bereichs-LEDs an
# erzeugt Zufallswerte für Temperaturen  

class GPIOManager:


    senseHat = SenseHat()
    LEDarea1 = LED(17)
    LEDarea2 = LED(27)
    LEDarea3 = LED(22)

    autoArea1 = False
    autoArea2 = False
    autoArea3 = False
    

    # Statusvariablen des jeweiligen Bereichs 
    # ungerade Zahl: LED aus/ manueller Modus aus 
    # gerade Zahl:   LED an/ manueller Modus an 
    # staticAutomatic: für Zustand des Automatikmodus
    state1 = 1
    state2 = 1
    state3 = 1
    statusAutomatic = False
    
    #Konstruktor 
    def __init__(self):
        pass
    
    #liest den integrierten Sensor aus, rundet auf 1 Nachkommastelle und zeigt auf Display an 
    #übergebene Parameter:  keine 
    #Parameter:             temperature Temperaturwert
    #                       tempMessage Nachricht (Tempwert), die auf Display angezeigt wird 
    #Rückgabe:              Temperaturwert 
    def getTemperatureSenseHat(self): 
        temperature = self.senseHat.get_temperature() 
        temperature = round(temperature, 1)
        tempMessage = str(temperature)+ " C" 
        self.senseHat.show_message(tempMessage)
        return temperature

    #hört auf den Joystick zum Umschalten des Modus 
    #übergebene Parameter:  keine 
    #Parameter:             message2, messageNot, message1, message3, messageAuto: Nachrichten
    #                       um jeweiligen Betriebsmodus anzuzeigen 
    #                       state1, state2, state3 (Status des man. Modus/ LED)
    #Rückgabe:              keine           
    def getJoystickEvent(self): 
        for event in self.senseHat.stick.get_events():
            if (event.action == "pressed"): 
                if (event.direction == "up"):
                    message2 = "M-B2"
                    self.senseHat.show_message(message2)   
                    self.turnLEDOnOff(2, self.state2)
                    return 2
                elif (event.direction == "down"):
                    messageNot = "N/A"
                    self.senseHat.show_message(messageNot)     
                elif (event.direction == "left"): 
                    message1 = "M-B1"
                    self.senseHat.show_message(message1)
                    self.turnLEDOnOff(1, self.state1)  
                    return 1
                elif (event.direction == "right"):
                    message3 = "M-B3"
                    self.senseHat.show_message(message3) 
                    self.turnLEDOnOff(3, self.state3)
                    return 3 
                elif (event.direction == "middle"):
                    messageAuto = "Automatik"
                    self.senseHat.show_message(messageAuto) 
                    self.turnOffAllLEDs(self.state1, self.state2, self.state3)

    #schaltet LED an oder aus 
    #übergebene Parameter:  area Zahl 1-3 für Bereich 1-3 
    #                       stateArea Zustand (Zahl durch 2 mit oder ohne Rest teilbar)
    #Sonstiges              mit Rest: LED soll angeschaltet werden
    #                       ohne Rest: LED soll ausgeschaltet werden 
    #Rückgabe:              keine  
    def turnLEDOnOff(self, area, stateArea):
        if (area == 1):
            if (stateArea % 2 == 1):
                self.state1 = self.state1 +1
                self.LEDarea1.on()
            elif (stateArea % 2 == 0):
                self.state1 = self.state1 +1 
                self.LEDarea1.off()
        elif (area == 2): 
            if (stateArea % 2 == 1):
                self.state2 = self.state2 +1 
                self.LEDarea2.on()
            elif (stateArea % 2 == 0):
                self.state2 = self.state2 +1
                self.LEDarea2.off()
        elif (area == 3): 
            if (stateArea % 2 == 1):
                self.state3 = self.state3 +1
                self.LEDarea3.on()
            elif (stateArea % 2 == 0):   
                self.state3 = self.state3 +1 
                self.LEDarea3.off()

    #wenn manueller Modus überall ausgeschaltet
    #übergebene Parameter:  state1, state2, state3 (Status des man. Modus/ LED)
    #Sonstiges              ohne Rest: LED soll ausgeschaltet werden 
    #                       wenn vorher nicht jeder Bereich manuell ausgeschaltet wurde, 
    #                       Ausgabe: nicht möglich                      
    #Rückgabe:              keine                  
    def turnOffAllLEDs(self, state1, state2, state3):
        if (state1 % 2 == 0 and state2 % 2 == 0 and state3 % 2 == 0):
            self.autoArea1 = True 
            self.autoArea2 = True 
            self.autoArea3 = True 
        else: 
            self.senseHat.show_message("nicht moeglich")
    
    #Dummywerte für Temperaturen erzeugen als Zufallswerte
    #übergebene Parameter:  keine
    #Rückgabe:              erzeugter Wert
    def getTemperature(self) -> float:
        temperature = random.uniform(-10,30)
        return temperature