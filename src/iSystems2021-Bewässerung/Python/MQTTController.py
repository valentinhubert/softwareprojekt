#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: MQTTController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält Klasse des MQTT-Managers 
#------------------------------------------------------------ 
import paho.mqtt.client
import paho.mqtt.client as mqttlib
import logging
from DatabaseController import DatabaseManager
from AutomaticController import AutomaticManager
import configparser
from GPIOController import GPIOManager
 

class MQTTManager:

    GPIOMan = GPIOManager()
    dbMan = DatabaseManager()
    autoMan = AutomaticManager()
    config = configparser.ConfigParser()
    config.read('config.ini')

    autoArea1 = False
    autoArea2 = False
    autoArea3 = False


    # MQTT Topics und URL definiert 
    MQTT_URL = config.get("MQTT", "url")
    MQTT_SEND_TOPIC_WINTER = "mqtt-to-ws/Winter-aktiv"
    MQTT_SEND_TOPIC = "bewaesserung/mqtt-to-ws"
    MQTT_SEND_TOPIC_NOT_WINTER = "mqtt-to-ws/nicht-Winter"
    MQTT_RECEIVE_TOPIC = "bewaesserung/ws-to-mqtt"
    
    # Initialisiert logger und MQTT-Client 
    # übergebene Parameter: keine 
    # Rückgabewerte: keine
    # Sonstiges: Rückmeldung ob Datenbankverbindung erfolgreich
    def __init__(self):
        self.__logger = logging.getLogger('DemoApp')
        self.__mqttclient = mqttlib.Client()
        self.__mqttclient.on_connect = self.mqtt_on_connect
        self.__mqttclient.message_callback_add(self.MQTT_RECEIVE_TOPIC, self.on_message_from_ui)
        self.__connected = False

    #Destruktor 
    def __del__(self):
        try:
            self.__mqttclient.disconnect()
            del self.__mqttclient
        except AttributeError:
            pass
        del self.__connected
        del self.__logger

    # Verbindet diesen MQTT-Client mit Server 
    # übergebene Parameter: keine 
    # Rückgabewerte: false bei Fehler 
    # Sonstiges: Rückmeldung ob Verbindung erfolgreich
    def connect_to_mqtt(self):
        try:
            self.__mqttclient.connect(self.MQTT_URL)
        except ConnectionError as e:
            self.__logger.error("Es konnte keine Verbindung mit MQTT hergestellt werden.")
            return False
        else:
            # Starten des MQTT Klienten
            self.__mqttclient.loop_start()
            self.__connected = True
            self.__logger.info(f"Verbindung zu MQTT-Server ({self.MQTT_URL}) erfolgreich hergestellt.")

    # Fängt Nachrichten von Benutzeroberfläche mittels MQTT ab 
    # übergebene Parameter: Nachrichteninhalt, von wem Nachricht (client)
    # Rückgabewerte: keine
    # Sonstiges: Rückmeldung ob Datenbankverbindung erfolgreich
    def on_message_from_ui(self, client, userdata, message: paho.mqtt.client.MQTTMessage):
        # Konvertiere den Inhalt der Nachricht in einen String
        payload = str(message.payload.decode())
        self.__logger.info(f'Neue Nachricht von UI: {payload}')
        print(payload)
        if (payload == "AutoOn"):
            self.autoArea1 = True
            self.autoArea2 = True
            self.autoArea3 = True
        elif (payload == "AutoOff"): 
            self.autoArea1 = False
            self.autoArea2 = False
            self.autoArea3 = False
        elif (payload == "Area1On"):         
            self.GPIOMan.turnLEDOnOff(1, self.GPIOMan.state1)
        elif (payload == "Area2On"):
            self.GPIOMan.turnLEDOnOff(2, self.GPIOMan.state2)  
        elif (payload == "Area3On"):
            self.GPIOMan.turnLEDOnOff(3, self.GPIOMan.state3)
        elif (payload == "Area1Off"):
            self.GPIOMan.turnLEDOnOff(1, self.GPIOMan.state1)
        elif (payload == "Area2Off"):
            self.GPIOMan.turnLEDOnOff(2, self.GPIOMan.state2)        
        elif (payload == "Area3Off"):
            self.GPIOMan.turnLEDOnOff(3, self.GPIOMan.state3)
        elif (payload == "Logout"):
            print("Erfolgreich ausgeloggt")
        

    # MQTT erfolgreich verbunden, abonniert Topic   
    # übergebene Parameter: client
    # Rückgabewerte: keine
    # Sonstiges: Rückmeldung ob Subscription erfolgreich 
    def mqtt_on_connect(self, client, userdata, flags, rc):
        self.__logger.info('MQTT Client erfolgreich verbunden') 
        try:
            client.subscribe(self.MQTT_RECEIVE_TOPIC)
        except:
            self.__logger.error("Falsches oder ungültiges MQTT Thema.")
        else:
            self.__logger.info("MQTT Thema {} nun abonniert.".format("bewaesserung/ws-to-mqtt"))
    
    # Publisht MQTT-Topic Wintermodus    
    # übergebene Parameter: keine
    # Rückgabewerte: keine
    # Sonstiges: nicht funktional - benötigt weitere Verbesserung/ Implementierung 
    def sendWinterMQTT(self):
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC_WINTER, "Wintermodus aktiviert")
        self.GPIOMan.LEDarea1.off()
        self.GPIOMan.LEDarea2.off()
        self.GPIOMan.LEDarea3.off()

    # Publisht MQTT-Topic Wintermodus ausgeschaltet    
    # übergebene Parameter: keine
    # Rückgabewerte: keine
    # Sonstiges: nicht funktional - benötigt weitere Verbesserung/ Implementierung 
    def sendNotWinterMQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC_NOT_WINTER, "Wintermodus deaktiviert")
        self.GPIOMan.state1 = 1
        self.GPIOMan.state2 = 1
        self.GPIOMan.state3 = 1

    # Senden der jeweiligen Messages ans Frontend für die Websites
    # Übergebene Parameter: keine    
    # Rückgabe: keine
    def sendStartLED1MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "startB1")

    def sendStopLED1MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "stopB1")

    def sendStartLED2MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "startB2")

    def sendStopLED2MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "stopB2")

    def sendStartLED3MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "startB3")

    def sendStopLED3MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "stopB3")

    def sendAutoStartLED1MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStartB1")

    def sendAutoStopLED1MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStopB1")

    def sendAutoStartLED2MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStartB2")

    def sendAutoStopLED2MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStopB2")

    def sendAutoStartLED3MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStartB3")

    def sendAutoStopLED3MQTT(self): 
        self.__mqttclient.publish(self.MQTT_SEND_TOPIC, "autoStopB3")

    # Prüft ob eine Mqtt verbindung aufgebaut ist 
    # Übergebene Parameter: keine
    # Rückgabe: True wenn aufgebaut werden konnte
    def start(self):
        if (self.__connected == True): 
            print("Verbindung MQTT erfolgreich")
            return True
