#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: MailController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält die Klasse zum Senden der Frostwarnungsmail
#------------------------------------------------------------
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from DatabaseController import DatabaseManager

# Klasse zum Senden der Frostwarnung via E-Mail an hinterlegte Nutzer 
# Funktionalitäten: schickt Mail an alle Nutzer aus der Datenbank 
#                   schickt Mail (testweise) an einen fest codierten Benutzer
class MailManager: 
    dbMan = DatabaseManager()
    # Inhalt der Mail als Text 
    mailtext = ("Hallo Benutzer!" + 
                "\n Der Temperatursensor hat eine Temperatur von unter 5 Grad festgestellt. Es könnte Frost geben.\n"+
                "Mit freundlichen Grüßen \n" + 
                "Ihr H2L Team \n \n" + 
                "Bei Fragen könnten Sie sich an uns wenden, aber wir können Ihnen da auch nicht helfen\n" + 
                "Also fragen Sie uns lieber nicht")
    # liest Mailadressen aus DB aus und schickt eine Frostwarnung an jede dieser Adressen  
    # übergebene Parameter: keine 
    # Parameter in Funktion: server = Webserver von web.de
    #                        mailElement = ein langer String mit allen Mailadressen aus DB 
    #                        seperatorCounter = zählt @-Zeichen, so oft wird String aufgetrennt
    #                        oneMail = Mails die an § Zeichen aufgetrennt wurden 
    #                        i = Iterator um alle Mails durchzugehen
    #                        mail = Inhalt, Betreff, Absender, Adressat werden zugewiesen 
    def sendFrostWarning(self): 
        server = smtplib.SMTP(host = 'smtp.web.de', port = 587)
        server.starttls()
        server.login('gruppe22.h2l.bewaesserung@web.de', 'sicheresPasswort#22', * '', initial_response_ok=True)
        mails = self.dbMan.getMail()
        for mail in mails:
            mailWarning = MIMEText(self.mailtext)
            mailWarning['subject'] = "Frostwarnung"
            mailWarning['From'] = "gruppe22.h2l.bewaesserung@web.de"
            mailWarning ['To'] = mail
            server.send_message(mailWarning)
        
    # (testweise) schickt Frostwarnung an eine fest vorgegebene Adresse  
    # übergebene Parameter: keine 
    # Parameter in Funktion: server = Webserver von web.de
    #                        mail = Inhalt, Betreff, Absender, Adressat werden zugewiesen 
    def testSendFrostWarning(self): 
        server = smtplib.SMTP(host = 'smtp.web.de', port = 587)
        server.starttls()
        server.login('gruppe22.h2l.bewaesserung@web.de', 'sicheresPasswort#22', * '', initial_response_ok=True)
        mail = MIMEText(self.mailtext)
        mail['subject'] = "Frostwarnung"
        mail['From'] = "gruppe22.h2l.bewaesserung@web.de"
        mail ['To'] = "lisahickl@web.de"
        server.send_message(mail)
        server.quit()
