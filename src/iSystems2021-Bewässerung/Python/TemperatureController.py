#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: TemperatureController.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Enthält die Klasse des Temperatur-Managers
#------------------------------------------------------------

from GPIOController import GPIOManager
from MailController import MailManager
from DatabaseController import DatabaseManager
from MQTTController import MQTTManager


# Klasse für Verarbeiten der Temperaturwerte 
# Funktionalitäten: holt sich Temperaturwerte 
class TemperatureManager: 
    GPIOMan = GPIOManager()
    MQTTMan = MQTTManager()
    dbMan = DatabaseManager()
    mailMan = MailManager()
    
    # skipFrostwarning für das einmalige senden der email 
    # Mode Somme für das einmalige umspringen des Betriebsmodus
    skipFrostwarning = 0
    mode = "sommer" 

    # liest Temperatur-/ Sensordaten aus und prüft "Wintermodus" und "Frost" ab und gibt diesen im Terminal aus 
    # übergebene Parameter: keine 
    # Parameter in Funktion: temperatureNow = aktueller Temperaturwert
    # Rückgabewerte: 1 für in Wintermodus geschalten 
    #                0 für Wintermodus deaktiviert
    def getTempData(self): 
        temperatureNow = self.GPIOMan.getTemperatureSenseHat()
        if (self.tempCompare(temperatureNow)):
            self.dbMan.pushTemp(temperatureNow)  
        if (temperatureNow < 15 and self.mode != "winter"):
            self.mode = "winter"
            self.MQTTMan.sendWinterMQTT()
            if (temperatureNow < 5):   
                self.mailMan.sendFrostWarning()
            return 1
        elif (temperatureNow < 5):
            if (self.skipFrostwarning < 1):   
                self.mailMan.sendFrostWarning()
                self.skipFrostwarning = self.skipFrostwarning + 1
            return 2
        elif (temperatureNow >=15 and self.mode != "sommer"): 
             self.mode = "sommer"
             self.MQTTMan.sendNotWinterMQTT()
             return 0

    # Vergleicht ob der letzte Temperaturwert in Datenbank ungleich aktuellem Wert w
    # Parameter: aktuelle Temperaturwert 
    # Rückgabe: True, wenn ungleich
    def tempCompare(self, temperatureNow):
        lastTemp = self.dbMan.getLastTemp()
        if(lastTemp != temperatureNow):
            return True
        else:
            return False


