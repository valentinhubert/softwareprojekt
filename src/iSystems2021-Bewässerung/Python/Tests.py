#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: Tests.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Hier werden einige Funktionen getestet 
#------------------------------------------------------------

from MailController import MailManager
from TemperatureController import TemperatureManager
from GPIOController import GPIOManager
from AutomaticController import AutomaticManager



if __name__ == "__main__":

     #holt alle E-Mails aus der DB raus und schickt Frostwarung an alle hinterlegten
     #MailManager.sendFrostWarning(MailManager)
     #holt einmalig einen Temperaturwert, schreibt in DB und prüft auf Winter und Frost 
     #TemperatureManager.GetTempData(TemperatureManager)
     while 1: 
          GPIOManager.getTemperatureSenseHat(GPIOManager)
          GPIOManager.getJoystickEvent(GPIOManager)
          #ThreadManager.startHardwareThread(ThreadManager)
     # hallo = AutomaticManager.mainfunction(AutomaticManager, 1)
     # print(hallo)

