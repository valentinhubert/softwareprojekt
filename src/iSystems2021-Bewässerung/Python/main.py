#------------------------------------------------------------
# Projekt: Bewaesserungssteuerung iSystems2021 - H2L
# Dateiname: Hauptprogramm.py
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Hauptprogramm für konstantes Abfragen der Temperaturen 
#------------------------------------------------------------
import time
from MQTTController import MQTTManager
from TemperatureController import TemperatureManager
from DatabaseController import DatabaseManager
from GPIOController import GPIOManager
from AutomaticController import AutomaticManager

if __name__=="__main__":
    MQTTMan = MQTTManager()
    tempMan = TemperatureManager() 
    GPIOMan = GPIOManager()
    dbMan = DatabaseManager()
    autoMan = AutomaticManager()
    
    MQTTMan.__init__()
    MQTTMan.connect_to_mqtt()

    
    # Endlosschleife zum ständigen Abfragen der Temperaturwerte (alle paar sekunden)
    # Es wird dauerhaft überprüft ob die eingeschriebenen Temperaturwerte älter als 30 d sind 
    # schickt Wintermodus aktiv oder deaktiverit ins Frontend
    # Prüft ob Knopf auf der Hardware gedrückt wurde 
    # returnedMode = 0 oder 1 (Winter nicht aktiv oder aktiv)
    if (MQTTMan.start()):
        while(1): 
            # Ab hier manueller Modus   
            hardwareEvent= GPIOMan.getJoystickEvent()
            if (hardwareEvent == 1):
                if (GPIOMan.state1 % 2 == 1):
                     MQTTMan.sendStopLED1MQTT()
                elif (GPIOMan.state1 % 2 == 0):
                    MQTTMan.sendStartLED1MQTT()     
            elif (hardwareEvent == 2):
                if (GPIOMan.state2 % 2 == 1):
                    MQTTMan.sendStopLED2MQTT()
                elif (GPIOMan.state2 % 2 == 0):
                    MQTTMan.sendStartLED2MQTT()
            if (hardwareEvent == 3):
                if (GPIOMan.state3 % 2 == 1):
                     MQTTMan.sendStopLED3MQTT()
                elif (GPIOMan.state3 % 2 == 0):
                    MQTTMan.sendStartLED3MQTT()
            # Ab hier wird Wintermouds behandelt
            returnedMode = tempMan.getTempData()
            if (returnedMode == 1): 
                MQTTMan.sendWinterMQTT()
            elif (returnedMode == 0):
                MQTTMan.sendNotWinterMQTT()
            # Hier werden die Temperaturen abgeglichen und ggf. gelöscht
            dbMan.deleteData()
            # Ab hier wird der Automatikmodus behandlet
            if (MQTTMan.autoArea1 == True or GPIOMan.autoArea1 == True): 
                check1 = autoMan.mainfunction(1)
                if (check1 == 1):
                    GPIOMan.LEDarea1.on()
                    MQTTMan.sendAutoStartLED1MQTT()
                elif (check1 == 2):
                    GPIOMan.LEDarea1.off()
                    MQTTMan.sendAutoStopLED1MQTT()
            if (MQTTMan.autoArea2 == True or GPIOMan.autoArea2 == True): 
                check2 = autoMan.mainfunction(2)
                if (check2 == 1):
                    GPIOMan.LEDarea2.on()
                    MQTTMan.sendAutoStartLED2MQTT()
                elif (check2 == 2):
                    GPIOMan.LEDarea2.off() 
                    MQTTMan.sendAutoStopLED2MQTT()
            if (MQTTMan.autoArea3 == True or GPIOMan.autoArea3 == True): 
                check3 = autoMan.mainfunction(3)
                if (check3 == 1):
                    GPIOMan.LEDarea3.on()
                    MQTTMan.sendAutoStartLED3MQTT()
                elif (check3 == 2):
                    GPIOMan.LEDarea3.off()   
                    MQTTMan.sendAutoStopLED3MQTT()
            time.sleep(1)
    else:
        print("MQTT-Verbindung konnte nicht aufgebaut werden")
