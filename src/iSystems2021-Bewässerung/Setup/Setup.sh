#!/bash/sh
#------------------------------------------------------------
# Projekt: Bewaesserung iSystems2021 - H2L
# Dateiname: Setup.sh
# Autor: Softwareentwicklungsgruppe 22
# Beschreibung: Dieses Shell-Skript installiert und initialisiert alle nötigen Komponenten
#------------------------------------------------------------

echo 'Lade System-Aktualisierungen herunter...\n\n'
sudo apt-get update
echo '\n\nSystem-Aktualisierungen erfolgreich heruntergeladen\n\n'


echo 'Installiere System-Aktualisierungen...\n'
echo 'Dies kann einen Moment dauern...\n\n'
sudo apt-get upgrade -y
echo 'System wurde erfolgreich aktualisiert!\n\n'

echo 'Initalisiere Installation der Softwarekomponenten der Bewaesserungssteuerung...\n\n'
echo 'Aktualisiere Paketlisten...\n'
sudo -E apt update -qq 
echo 'Installiere Maria DB...\n'
sudo -E apt install -y mariadb-server
echo 'Setze root und Root-Passwort...\n'
sudo mysql -u root --execute="ALTER USER 'root'@'localhost' IDENTIFIED BY '123456';"
echo 'Initialisiere Datenbank...\n\n'
mysql --host=localhost --user=root --password=root < DatabaseInit.sql
echo '\nDatenbank initialisiert!\n\n'

echo 'MQTT-Broker (Mosquitto) installieren...\n'
sudo -E apt install -y mosquitto mosquitto-clients 
echo 'NodeJS (und npm) installieren...\n'
sudo -E apt install -y nodejs npm
echo 'Python Paketmanager (pip) installieren...\n'
sudo -E apt install -y python3-pip 
echo 'Python MariaDB Connector installieren...\n'
pip3 install mariadb 
echo 'Python Paho-MQTT installieren...\n'
pip3 install paho-mqtt 
echo 'Hardwarebibliotheken installieren\n'
echo 'SenseHat Modul installieren...\n'
sudo apt install sense-hat
echo 'Bibliothek für die Ansteuerung der LEDs installieren...\n'
sudo apt install python3-gpiozero
echo 'Ins richtige Verzeichnis gehen und Webserver starten...\n' 
cd ~/softwareprojekt/src/iSystems2021-Bewässerung
echo 'Restliche Pakete installieren'
npm install
echo 'MQTT Broker starten'
mosquitto -d