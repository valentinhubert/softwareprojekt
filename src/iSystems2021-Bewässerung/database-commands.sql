/* Erstelle die Datenbank */
CREATE DATABASE WebMessages;

/* Auswahl der Datenbank */
USE WebMessages;

/* Erstelle die Datenbank Tabelle */
CREATE TABLE `Messages` (
	`ID` INT NOT NULL AUTO_INCREMENT,
	`TimeStamp` DATETIME DEFAULT NOW(),
	`Message` TEXT,
	`Topic` TEXT,
	PRIMARY KEY (`ID`)
);

/* Beispiel Statement zum Einfügen der Daten */
INSERT INTO Messages(Topic, Message) VALUES('123', 'Hello World');

/* Beispiel Statement zum Abrufen der Daten */
SELECT * FROM Messages ORDER BY TimeStamp DESC;