// ------------------------------------------------------------
//  Projekt: Bewaesserungssteuerung iSystems2021 - H2L
//  Dateiname: Login.js
//  Autor: Softwareentwicklungsgruppe 22
//  Beschreibung: Enthält das dynamische Handling der Login-Seite   
// ------------------------------------------------------------ 

const hostname = window.location.hostname;
const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);

// Alle Benutzereingaben und buttons definieren für anschließende Logik
// Login-Eingaben
const LoginBlockLeftText = document.getElementById("LoginBlockLeftText");
const LoginBlockRight = document.getElementById("LoginBlockRight");
const LoginButton = document.getElementById("LoginButton");
const LoginInputUser = document.getElementById("LoginInputUser");

//Funktion, welche beim erfolgreichen Öffnen des Websockets abgearbeitet wird
ws.onopen = function () 
{
  console.log(`Erfolgreich mit WebSocket verbunden. (URL = ${webSocketUrl})`);
};
  
 
// Funktion die bei einer neuen Nachricht ausgeführt wird.
// @param messageEvent Event mit neuer Nachricht.
ws.onmessage = function (messageEvent) 
{
  console.log(`Neue Nachricht empfangen "${messageEvent.data}"`);
};

//Logik für Login
LoginButton.onclick = ()=>
{
  stringLoginButton="stringLoginButton";
  console.log("Login Knopf gedrueckt");
  ws.send(LoginInputUser.value+"§"+stringLoginButton);
  ws.send("LoginButton")
}

//Zeige aktuelles Datum auf der HTML Oberfläche an 
function date()
{
  const date = new Date();
  dataTime = date.toLocaleString();
  dataTime = dataTime.slice(0,9);
  document.getElementById("date").innerHTML = dataTime; 
}

