// ------------------------------------------------------------
//  Projekt: Bewaesserungssteuerung iSystems2021 - H2L
//  Dateiname: main.js
//  Autor: Softwareentwicklungsgruppe 22
//  Beschreibung: Enthält Dynamik der Hauptseite der Bewaesserungssteuerung   
// ------------------------------------------------------------ 
const hostname = window.location.hostname;
const webSocketUrl = `ws://${hostname}:8999`;
const ws = new WebSocket(webSocketUrl);

// Alle Benutzereingaben und Buttons definieren für anschließende Logik 
// Bewässerungseingaben
const FromTimeB1 = document.getElementById("FromTimeB1");
const TilTimeB1 = document.getElementById("TilTimeB1");
const FromTimeB2 = document.getElementById("FromTimeB2");
const TilTimeB2 = document.getElementById("TilTimeB2");
const FromTimeB3 = document.getElementById("FromTimeB3");
const TilTimeB3 = document.getElementById("TilTimeB3");
const AutoOn = document.getElementById("AutoOn");
const AutoOff = document.getElementById("AutoOff");
const Area1On = document.getElementById("Area1On");
const Area1Off = document.getElementById("Area1Off");
const Area2On = document.getElementById("Area2On");
const Area2Off = document.getElementById("Area2Off");
const Area3On = document.getElementById("Area3On");
const Area3Off = document.getElementById("Area3Off");
const Logout = document.getElementById("Logout");
const showHidePicArea1 = document.getElementById("PicArea1");
const showHidePicArea2 = document.getElementById("PicArea2");
const showHidePicArea3 = document.getElementById("PicArea3");

//  Globale Variable für den Manuellen Modus 
//  0 = Betriebsmodus auswählen
//  1 = Bereich1 Manuell
//  2 = Bereich2 Manuell
//  3 = Bereich3 Manuell
//  4 = Automatik
//  5 = Wintermodus

 modeArea1 = 0;
 modeArea2 = 0;
 modeArea3 = 0;
 modeAuto = 0; 
 modeWinter = 0;

//  Logik für die Bewässerung im Automatikmodus 
//  Zeitwert wird mit erkennungsstring an webserver gesendet
//  Im webserver wird dadurch unterschieden

AutoOn.onclick = () =>
{
    modeAuto = 1;

    stringFromTimeB1="stringFromTimeB1";
    ws.send(FromTimeB1.value+stringFromTimeB1);
    console.log("Von Zeit Bereich 1 gesendet");

    stringTilTimeB1="stringTilTimeB1";
    ws.send(TilTimeB1.value+stringTilTimeB1);
    console.log("Bis Zeit Bereich 1 gesendet");

    stringFromTimeB2="stringFromTimeB2";
    ws.send(FromTimeB2.value+stringFromTimeB2);
    console.log("Von Zeit Bereich 2 gesendet");

    stringTilTimeB2="stringTilTimeB2";
    ws.send(TilTimeB2.value+stringTilTimeB2);
    console.log("Bis Zeit Bereich 2 gesendet");

    stringFromTimeB3="stringFromTimeB3";
    ws.send(FromTimeB3.value+stringFromTimeB3);
    console.log("Von Zeit Bereich 3 gesendet");

    stringTilTimeB3="stringTilTimeB3";
    ws.send(TilTimeB3.value+stringTilTimeB3);
    console.log("Bis Zeit Bereich 3 gesendet");

    ws.send("AutoOn");
    console.log("Automatik Ein Knopf gedrückt");

    showMode();
    hideManuel();
    AutoOn.style.visibility="hidden";
}

AutoOff.onclick = () =>
{
    modeAuto = 0;
    ws.send("AutoOff");
    console.log("Automatik ausgeschaltet");
    
    showMode();
    showManuel();
    showAutomatic();   
}

//  Anzeigelogik für die Bewässerung im Manuellen Modus  
//  Automatik Modus ist ausgeschaltet  

//  Manueller Bewässerungsbereich 1
Area1On.onclick = () =>
{   
    modeArea1 = 1;
    ws.send("Area1On");
    console.log("Bereich 1 Manuell eingeschaltet");
    

    showMode();
    hideAutomatic();
    Area1On.style.visibility="hidden";
    showHidePicArea1.style.visibility="visible"; 
}

Area1Off.onclick = () =>
{
    modeArea1 = 0;
    ws.send("Area1Off");
    console.log("Bereich 1 Manuell ausgeschaltet");
    Area1On.style.visibility="visible";
    showHidePicArea1.style.visibility="hidden";

    showMode();
    if(checkAllManual()==true)
    {
        showAutomatic();
    }
}

// Manueller Bewässerungsbereich 2
Area2On.onclick = () =>
{
    modeArea2 = 1;
    ws.send("Area2On");
    console.log("Bereich 2 Manuell eingeschaltet");
    
    showMode();
    hideAutomatic();
    Area2On.style.visibility="hidden";
    showHidePicArea2.style.visibility="visible"; 
}

Area2Off.onclick = () =>
{
    modeArea2 = 0;
    ws.send("Area2Off");
    console.log("Bereich 2 Manuell ausgeschaltet");
    Area2On.style.visibility="visible";
    showHidePicArea2.style.visibility="hidden";

    showMode();
    if(checkAllManual()==true)
    {
        showAutomatic();
    }
}

// Manueller Bewässerungsbereich 3
Area3On.onclick = () =>
{
    modeArea3 = 1;
    ws.send("Area3On");
    console.log("Bereich 3 Manuell eingeschaltet");
    
    showMode();
    hideAutomatic();
    Area3On.style.visibility="hidden";
    showHidePicArea3.style.visibility="visible"; 
}

Area3Off.onclick = () =>
{
    modeArea3 = 0;
    ws.send("Area3Off");
    console.log("Bereich 3 Manuell ausgeschaltet");
    Area3On.style.visibility="visible";
    showHidePicArea3.style.visibility="hidden";

    showMode();
    if(checkAllManual()==true)
    {
        showAutomatic();
    }
}

// Logoutknopf 
Logout.onclick = () =>
{
    ws.send(Logout);
    console.log("Ausgeloggt")
}

//   Zeige aktuelles Datum auf der HTML Oberfläche an 
//   Parameter: keine 
//   Rückgabe: keine
function date()
{
     const date = new Date();
     dataTime = date.toLocaleString();
     dataTime = dataTime.slice(0,9);
     document.getElementById("date").innerHTML = dataTime; 
 }

 

//  Zeigt den aktuellen Modus auf der Website an 
//  Parameter: keine 
//  Rückgabe: keine
function showMode()
{
    if(modeArea1 || modeArea2 || modeArea3 == 1)
    {
        document.getElementById("msg").innerHTML = "Manuellmodus eingeschalten";
    }
    else if(modeAuto == 1)
    {
        document.getElementById("msg").innerHTML = "Automatikmodus eingeschalten";
    }
    else if(modeWinter == 1)
    {
        document.getElementById("msg").innerHTML = "Wintermodus aktiv";
    }
    else
    {
        document.getElementById("msg").innerHTML = "Bitte Betriebsmodus wählen";
    }
}

//  Um alle Automatikknöpfe sichtbar zu machen 
//  Parameter: keine 
//  Rückgabe: keine
function showAutomatic()
{
    AutoOn.style.visibility ="visible";
    AutoOff.style.visibility="visible";
}

//  Um alle Automatikknöpfe unsichtbar zu machen
//  Parameter: keine 
//  Rückgabe: keine
function hideAutomatic()
{
    AutoOn.style.visibility = "hidden";
    AutoOff.style.visibility = "hidden";
}

// Um alle Manuellen Knöpfe unsichtbar zu machen
//  Parameter: keine 
//  Rückgabe: keine

function hideManuel()
{
    Area1On.style.visibility="hidden";
    Area1Off.style.visibility="hidden";

    Area2On.style.visibility="hidden";
    Area2Off.style.visibility="hidden";

    Area3On.style.visibility="hidden";
    Area3Off.style.visibility="hidden";
}

//  Um alle Manuellen Knöpfe sichtbar zu machen 
//  Parameter: keine 
//  Rückgabe: keine
 function showManuel() 
{
    Area1On.style.visibility="visible";
    Area1Off.style.visibility="visible";

    Area2On.style.visibility="visible";
    Area2Off.style.visibility="visible";

    Area3On.style.visibility="visible";
    Area3Off.style.visibility="visible";
}

//  Um alle Bilder für die Bewaesserung unsichtbar zu machen 
//  Parameter: keine 
//  Rückgabe: keine
function hidePics()
{
    showHidePicArea1.style.visibility = "hidden";
    showHidePicArea2.visibility = "hidden";
    showHidePicArea3.visibility = "hidden";
}

//  Um zu prüfen, ob alle manuellen Bereiche ausgeschaltet sind 
//  Parameter: keine 
//  Rückgabe: true, wenn alle Bereiche ausgeschaltet sind
function checkAllManual()
{
    if(modeArea1 || modeArea2 || modeArea3 == 1)
    {
        return false;
    }
    else
    {
       return true;
    }
}

 
//   Funktion die bei einer neuen Nachricht ausgeführt wird.
//   @param messageEvent Event mit neuer Nachricht.
//   Beinhaltet die Anzeigelogik der Website über Backend und webserver
 
ws.onmessage = ({data}) =>
{
    console.log(`Neue Nachricht empfangen "${data}"`);
   // addMessageToQueue(messageEvent.data);
    console.log(data); 
        
    switch (data.toString())
    {
        case "Wintermodus aktiviert":
            modeWinter=1;
            hideAutomatic();
            hideManuel();
            hidePics();
            showMode();
            break;

        case "Wintermodus deaktiviert":
            modeWinter=0;
            showAutomatic();
            showManuel();
            showMode();
            break;

        case "startB1":
            modeArea1 = 1;
            showMode();
            hideAutomatic();
            Area1On.style.visibility="hidden";
            showHidePicArea1.style.visibility="visible"; 
            break;

        case "startB2":
            modeArea2=1;
            showMode();
            hideAutomatic();
            Area2On.style.visibility="hidden";
            showHidePicArea2.style.visibility="visible"; 
            break;

        case "startB3":
            modeArea3 = 1;
            showMode();
            hideAutomatic();
            Area3On.style.visibility="hidden";
            showHidePicArea3.style.visibility="visible"; 
            break;

        case "stopB1":
            modeArea1 = 0;
            Area1On.style.visibility="visible";
            showHidePicArea1.style.visibility="hidden";
            showMode();
            if(checkAllManual()==true)
            {
                showAutomatic();
            }
            break;
            
        case "stopB2":
            modeArea2 = 0;
            Area2On.style.visibility="visible";
            showHidePicArea2.style.visibility="hidden";
            showMode();
            if(checkAllManual()==true)
            {
                showAutomatic();
            }
            break;
            
        case "stopB3":
            modeArea3 = 0;
            Area3On.style.visibility="visible";
            showHidePicArea3.style.visibility="hidden";
            showMode();
            if(checkAllManual()==true)
            {
                showAutomatic();
            }
            break;
        
        case "autoStartB1": 
            showHidePicArea1.style.visibility = "visible";
            break;
        
        case "autoStopB1":
            showHidePicArea1.style.visibility = "hidden";
            break;
        
        case "autoStartB2":
            showHidePicArea2.style.visibility="visible";
            break;

        case "autoStopB2":
            showHidePicArea2.style.visibility = "hidden";
            break;
        
        case "autoStartB3":
            showHidePicArea3.style.visibility="visible";
            break;
        
        case "autoStopB3":
            showHidePicArea3.style.visibility="hidden";
            break;
    }

}
