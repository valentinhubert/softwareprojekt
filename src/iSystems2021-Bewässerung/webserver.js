// ------------------------------------------------------------
//  Projekt: Bewaesserungssteuerung iSystems2021 - H2L
//  Dateiname: webserver.js
//  Autor: Softwareentwicklungsgruppe 22
//  Beschreibung: Enthält Webserver und Websocket-Konfiguration  
// ------------------------------------------------------------ 

//imports 
const express = require("express");
const http = require("http");
const WebSocket = require("ws");
const mqtt = require("mqtt");
const mariadb = require("mariadb");
var configuration = require('./config.json');

//Konfiguration der IP-Adresse, der Ports und MQTT-Topics 
//Irgendwas 
const webUIHost = "192.168.185.151";
const webUIPort = 3000;
const webSocketPort = 8999;
const mqttUrl = configuration.mqtt.url;
const mqttReceiveTopicWinter = "mqtt-to-ws/Winter-aktiv"; 
const mqttReceiveTopicNotWinter = "mqtt-to-ws/nicht-Winter"; 
const mqttReceiveTopic = "bewaesserung/mqtt-to-ws"
const mqttSendTopic = "bewaesserung/ws-to-mqtt"
const htmlFolderName = "html";
const databaseHost = configuration.mariadb.host;
const databaseName = configuration.mariadb.db;
const databaseUsername = configuration.mariadb.user;
const databasePassword = configuration.mariadb.password;

// Globale Objekte 
// Verbinde MQTTClient mit dem Server und initialisiere Websocket 
const app = express();
const webServer = http.createServer(app);
const mqttClient = mqtt.connect(mqttUrl);
const webSocketServer = new WebSocket.Server({ server: webServer });

// Datenbank Verbindung wird in Methode erzeugt
let databaseConnection = null;


  // Sende eine Nachricht an alle verbundenen WebSocket Clients.
  // Parameter: Nachricht/ Message
  // @param message Nachricht, welche versendet werden soll.
 
sendMessageToAllClients = (message) => 
{
  webSocketServer.clients.forEach((client) => 
  {
    client.send(message);
  });
};


  // Abonniert ein Topic mittels des MQTTClients
  // Parameter: topic 
  // @param topic Topic, welches abonniert werden soll.
 
mqttClientSubscribeToTopic = (topic) => 
{
  mqttClient.subscribe(topic, {}, (error, granted) => 
  {
    if (granted !== null) 
    {
      console.error(
        `Subscription erfolgreich erstellt. Topic = ${topic}`
      );
    } 
    else 
    {
      console.error("Subscription konnte nicht erstellt werden.");
    }
  });
};


// Verarbeite ankommende MQTT Nachricht und schickt diese weiter  
// @param topic Thema der ankommenden Nachricht
// @param message Nachricht Inhalt
 
handleMqttMessage = (topic, message) => 
{
  console.log(
    "Nachricht: %s",
    message.toString().trim()
  );
  sendMessageToAllClients(message.toString());
};


  // Verarbeite ankommende Websocket Nachricht prüft verschiedene Fälle ab
  // Parameter in Funktion: messageString = aufgetrennte Nachricht 
  //                        FindSign = sucht nach § Zeichen 
  //                        messageMail = weiter aufgetrennte Nachricht an §-Zeichen 
  //                        messagetest
  //                        hallo
  // @param message Nachricht welche empfangen wurde
 
handleWebSocketMessage = (message) => 
{
  mqttClient.publish(mqttSendTopic, message);  
  switch (messageString=message.slice(5,message.length))
  {
    case "stringFromTimeB1":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET FromTimeB1  = (?) WHERE id=355", (messageTime))
      

    case "stringTilTimeB1":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET TilTimeB1  = (?) WHERE id=355", (messageTime))
      
      
    case "stringFromTimeB2":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET FromTimeB2  = (?) WHERE id=355", (messageTime))
      
      
    case "stringTilTimeB2":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET TilTimeB2  = (?) WHERE id=355", (messageTime))
      
      
    case "stringFromTimeB3":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET FromTimeB3  = (?) WHERE id=355", (messageTime))
      
      
    case "stringTilTimeB3":
      messageTime = message.slice(0,5);
      databaseConnection.query(
        "UPDATE bewaesserung SET TilTimeB3  = (?) WHERE id=355", (messageTime)) 
      break;
  }

  if (message.indexOf("§")!=-1)
  {
    FindSign = message.indexOf("§");
    messageMail = message.slice(0,FindSign);
    messagetest = message.indexOf("@");
    if(messagetest!=-1)
    {
      databaseConnection.query(
        "INSERT INTO bewaesserung(Mail) VALUES(?)", (messageMail))
    }
  }  
};

// Erstelle Handler für MQTTClient und richte Subscription zu ausgewählten Themen ein
// Parameter: keine 
initMqttClient = () => 
{
  mqttClient.on("connect", () => 
  {
    console.log(`MQTT wurde erfolgreich verbunden. Adresse = ${mqttUrl}`);

    mqttClientSubscribeToTopic(mqttReceiveTopicNotWinter);
    mqttClientSubscribeToTopic(mqttReceiveTopicWinter); 
    mqttClientSubscribeToTopic(mqttReceiveTopic); 
  });
  mqttClient.on("message", (topic, message) =>
    handleMqttMessage(topic, message)
  );
  
};


//  Initialisiere Einstellungen des Webservers, setzt richtiges Verzeichnis für HTML, CSS ein 
 
initWebServer = () => 
{
  app.use(express.static(htmlFolderName));
  // GET Endpunkt für aktuelle Uhrzeit
  app.get("/api/now", (request, response) => 
  {
    return response.send(`${new Date()}`);
  });

  // GET Endpunkt für Liste mit letzten x Ergebnissen.
  app.get("/api/led-events", async (request, response) => 
  {
    const limit = Number(request.query.limit) || 10;
    let results = [];

    await databaseConnection
      .query(`SELECT * FROM Messages ORDER BY TimeStamp DESC LIMIT ${limit}`)
      .then((res) => (results = res));

    return response.send(results);
  });
};


  // Starte Webserver unter ausgewähltem Port
 
startWebServer = () => 
{
  app.listen(webUIPort, webUIHost, () => 
  {
    console.log(
      `WebServer wurde gestartet. Adresse = http://${webUIHost}:${webUIPort}`
    );
  });
};


  // Initialisiert Websocket-Server und legt fest, welche Funktion bei Erhalt 
  // einer Nachricht vom Websocket ausgeführt wird 
 
initWebsocketServer = () => 
{
  webSocketServer.on("connection", (ws) => 
  {
    ws.on("message", (message) => handleWebSocketMessage(message));
    ws.send("Verbindung erfolgreich.");
  });
};


  // Startet Websocket-Server unter ausgewähltem Port
  // Konsolenausgabe informiert über Start 
 
startWebSocketServer = () => 
{
  webServer.listen(webSocketPort, webUIHost, () => 
  {
    console.log(
      `WebSocket Server wurde gestartet. Adresse = http://${webUIHost}:${webSocketPort}`
    );
  });
};


// Verbinde zu Datenbank-Server
// wartet auf erfolgreiche Verbindung zur DB 
initDatabaseConnection = async () => 
{
  await mariadb
    .createConnection(
    {
      host: databaseHost,
      user: databaseUsername,
      password: databasePassword,
      database: databaseName,
    })
    .then((conn) => 
    {
      console.log(
        `Datenbank (Host: ${databaseHost}) Verbindung wurde erfolgreich aufgebaut`
      );
      databaseConnection = conn;
    })
    .catch(() => 
    {
      // Fehlerfall
      console.error(
        "Fehler. Datenbankverbindung konnte nicht aufgebaut werden."
      );
      process.exit();
    });
};


// Prüfe ob Datenbank erfolgreich verbunden und Datenbank sowie Tabelle korrekt angelegt sind.

checkDatabaseConnection = async () => 
{
  await databaseConnection
    .query("SELECT COUNT(*) as Count FROM bewaesserung")

    .then((results) =>
      console.log(
        `Test erfolgreich. Es wurden ${results[0].Count} Einträge gefunden!`
      )
    )
    
    .catch((error) => 
    {
      console.error(error);
      // Fehlerfall
      console.error(
        "Datenbank Test nicht erfolgreich. Abbruch! Bitte prüfen ob alle Tabellen angelegt sind."
      );
      process.exit();
    });
};


  // Main Methode initialisiert und startet Webserver und Websocket
  // baut Datenbankverbindung auf und prüft, ob dies erfolgreich war 
 
main = async () => 
{
  initMqttClient();

  await initDatabaseConnection();
  await checkDatabaseConnection();
 

  initWebServer();
  initWebsocketServer();
  

  startWebServer();
  startWebSocketServer();
};

// Führe Main-Funktion aus, als Standard-Methode
main().catch((error) => 
{
  console.error(error);
  process.exit();
});